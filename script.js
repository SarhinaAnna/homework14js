document.getElementById(`button`).addEventListener("click", function () {
  const existingThema = localStorage.getItem("thema"); //1.null 2.dark
  if (existingThema == null) {
    document.body.classList.add("dark");
    localStorage.setItem("thema", "dark");
  } else if (existingThema == "dark") {
    localStorage.removeItem("thema");
    document.body.classList.remove("dark");
  }
});

const existingThema = localStorage.getItem("thema"); //1.null 2.dark
if (existingThema == null) {
  document.body.classList.add("dark");
} else if (existingThema == "dark") {
  document.body.classList.add("dark");
}
